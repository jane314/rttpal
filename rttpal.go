package main

import (
	"encoding/binary"
	"fmt"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

func SendLoopPrimary(lconn *net.UDPConn, raddr *net.UDPAddr, latencyinfo map[int64]int64, wg *sync.WaitGroup) {
	defer wg.Done()
	data := make([]byte, 32)
	for {
		timenow := time.Now().UnixNano()
		latencyinfo[timenow] = time.Now().UnixNano()
		binary.LittleEndian.PutUint64(data, uint64(timenow))
		lconn.WriteTo(data, raddr)
		time.Sleep(10 * time.Second)
	}
}

func ReceiveLoopPrimary(lconn *net.UDPConn, laddr *net.UDPAddr, latencyinfo map[int64]int64, wg *sync.WaitGroup) {
	defer wg.Done()
	data := make([]byte, 8)
	for {
		n, _, err := lconn.ReadFromUDP(data)
		m, _ := binary.Varint(data)
		if err == nil {
			fmt.Println(n, "\t", m)
		}
	}
}

func SendAndReceiveLoopSecondary(lconn *net.UDPConn, laddr *net.UDPAddr, raddr *net.UDPAddr, wg *sync.WaitGroup) {
	data := make([]byte, 8)
	for {
		n, _, err := lconn.ReadFromUDP(data)
		if err == nil {
			fmt.Println(n)
			lconn.WriteTo(data, raddr)
		}
	}

}

func main() {
	if len(os.Args) < 4 || !(os.Args[1] == "primary" || os.Args[1] == "secondary") {
		log.Fatal("\n\tUSAGE: rttpal [primary|secondary] [Primary Hostname:Port] [Secondary Hostname:Port]\n")
	}
	laddr, err := net.ResolveUDPAddr("udp", os.Args[2])
	if err != nil {
		log.Fatal(err)
	}
	raddr, err := net.ResolveUDPAddr("udp", os.Args[3])
	if err != nil {
		log.Fatal(err)
	}
	lconn, err := net.ListenUDP("udp", laddr)
	if err != nil {
		log.Fatal(err)
	}
	defer lconn.Close()
	var wg sync.WaitGroup
	latencyinfo := make(map[int64]int64)
	if os.Args[1] == "primary" {
		wg.Add(2)
		go SendLoopPrimary(lconn, raddr, latencyinfo, &wg)
		go ReceiveLoopPrimary(lconn, laddr, latencyinfo, &wg)
	} else {
		wg.Add(1)
		go SendAndReceiveLoopSecondary(lconn, laddr, raddr, &wg)
	}
	wg.Wait()
}
